﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PlayGround.Models;

namespace PlayGround.Controllers
{
    [RoutePrefix("test/v2")]
    public class ValuesController : ApiController
    {
        private readonly IVehicle _vehicle;

        public ValuesController(IVehicle vehicle)
        {
            _vehicle = vehicle;
        }

        [Route("ping/{message}")]
        // GET api/values
        public string Get()
        {
            return _vehicle.Drive();
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
