﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PlayGround.Models;

namespace PlayGround.Controllers
{
    public class FileUploadController : Controller
    {
        private readonly IFileHandler _fileHandler;

        public FileUploadController(IFileHandler fileHandler)
        {
            _fileHandler = fileHandler;
        }
        // GET: FileUpload
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(IEnumerable<HttpPostedFileBase> files)
        {
            foreach (var file in files)
            {
                var fileName = file;
            }

            return View();
        }
    }
}