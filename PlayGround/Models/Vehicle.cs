﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace PlayGround.Models
{
    public class Vehicle : IVehicle
    {
        public string Drive()
        {
            return "Hurrah";
        }
    }

    public class File 
    {

    }

    public class FileHandler : IFileHandler
    {
        public List<string> ValidFileExtensions
        {
            get
            {
                List<string> extensions = new List<string>(GetAppSettings("ValidExtensions"));
                extensions.AddRange(GetAppSettings("ZipExtensions"));
                return extensions;
            }
        }

        public List<string> ZipExtensions
        {
            get
            {
                return new List<string>(GetAppSettings("ZipExtensions"));
            }
        }

        public string GetFolderFromPath(string path)
        {
            return Path.GetDirectoryName(path);
        }

        public bool FileHasValidExtension(string path)
        {
            return ValidFileExtensions.Any(item => path.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }

        public bool FileHasZipExtension(string path)
        {
            return ZipExtensions.Any(item => path.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }
        private string[] GetAppSettings(string key)
        {
            string appSettingsValue = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrEmpty(appSettingsValue))
            {
                return new string[0];
            }
            return appSettingsValue.Split(',');
        }

    }

    public interface IFileHandler
    {
    }
}