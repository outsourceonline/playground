﻿namespace PlayGround.Models
{
    public interface IVehicle
    {
        string Drive();
    }
}