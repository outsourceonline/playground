﻿using System.Collections.Generic;
using System.Web;

namespace FileUploader.Models
{
    public class UploadingFile
    {
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
        public long Length { get; set; }
        public IEnumerable<HttpPostedFileBase> Files { get; set; }
    }

    public static class ExtensionMethods
    {
        public static bool HasFile(this HttpPostedFileBase file)
        {
            return file != null && file.ContentLength > 0;
        }
    }
}