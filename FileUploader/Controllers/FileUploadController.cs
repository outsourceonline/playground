﻿using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using FileUploader.Models;

namespace FileUploader.Controllers
{
    public class FileUploadController : Controller
    {
        private readonly IFileHandler _fileHandler;

        public FileUploadController(IFileHandler fileHandler)
        {
            _fileHandler = fileHandler;
        }
        
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(IEnumerable<HttpPostedFileBase> files)
        {
            var model = new List<UploadingFile>();

            foreach (var file in files)
            {
                using (var inputStream = file.InputStream)
                {
                    if (file.HasFile())
                    {
                        var target = new MemoryStream();
                        file.InputStream.CopyTo(target);
                        byte[] data = target.ToArray();

                        model.Add(new UploadingFile
                        {
                            FileName = file.FileName,
                            ContentType = file.ContentType,
                            Content = data,
                        });
                    }
                }
            }

            return View(model);
        }
    }
}